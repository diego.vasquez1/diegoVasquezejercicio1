package org.example;


public class Autor extends  Persona {
    String descripcion;

    @Override
    public String toString() {
        return "Autor{" +
                " nombre="+super.getNombre()+
                " apellido="+super.getApellido()+
                " edad="+String.valueOf(super.getEdad())+
                "descripcion='" + descripcion + '\'' +
                '}';
    }

    public Autor(String nombre, String apellido, int edad, String descripcion) {
        super(nombre, apellido, edad);
        this.descripcion = descripcion;
    }

    public Autor() {
        this.descripcion = "";
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
