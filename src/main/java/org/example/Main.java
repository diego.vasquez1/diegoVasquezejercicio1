package org.example;

public class Main {
    public static void main(String[] args) {

        // Ejemplos de instanciación de objetos
        Autor a1 = new Autor("juan","vasquez",5,"es un autor");
        Autor a2 = new Autor("Gabriel ","García Márquez",512,"es un autor tambien");
        Libro libro1 = new Libro("El principito", a1, 1943);
        Libro libro2 = new Libro();
        libro2.setTitulo("Cien años de soledad");
        libro2.setAutor(a2);
        libro2.setAnioPublicacion(1967);
        Empleado empleado1 = new Empleado("Juan", "Pérez", 30,2000);
        Empleado empleado2 = new Empleado();

        empleado2.setApellido("González");
        empleado2.setEdad(25);





        Biblioteca biblioteca = new Biblioteca();
        biblioteca.addLibro(libro1);
        biblioteca.addLibro(libro2);
        biblioteca.addEmpleado(empleado1);
        biblioteca.addEmpleado(empleado2);


        System.out.println(biblioteca.calcularGastosSalarios());
        System.out.println(biblioteca.encontrarEmpleado("Juan"));

        System.out.println(biblioteca.encontrarLibro("El principito"));

    }
}