package org.example;

public class Libro {
    private String titulo;
    private Autor autor;
    private int anioPublicacion;


    @Override
    public String toString() {
        return "\nLibro{" +
                "titulo='" + titulo + '\'' +
                ", autor='" + autor + '\'' +
                ", anioPublicacion=" + anioPublicacion +
                '}';
    }

    // Constructor por parámetros
    public Libro(String titulo, Autor autor, int anioPublicacion) {
        this.titulo = titulo;
        this.autor = autor;
        this.anioPublicacion = anioPublicacion;
    }

    // Constructor por defecto
    public Libro() {
        this.titulo = "";
        this.autor = new Autor();
        this.anioPublicacion = 0;
    }

    // Getters y setters para cada atributo
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public int getAnioPublicacion() {
        return anioPublicacion;
    }

    public void setAnioPublicacion(int anioPublicacion) {
        this.anioPublicacion = anioPublicacion;
    }
}
