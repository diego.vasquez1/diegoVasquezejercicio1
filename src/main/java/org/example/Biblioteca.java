package org.example;

import java.util.*;
import java.util.stream.Collectors;

public class Biblioteca {
    public String nombre;
    public String direccion;

    Map<String,Libro> libros;
    List<Empleado> empleados;

    /*Búsqueda de empleados a partir de un nombre y pintar
    todos sus datos por pantalla. Tened en cuenta que puede existir más de un empleado con el
    mismo nombre. */
    public List<Empleado> encontrarEmpleado(String nombre){
        List<Empleado> empleadosConMismoNombre = empleados.stream()
                .filter(empleado -> nombre.equals(empleado.getNombre()))
                .collect(Collectors.toList());


        return empleadosConMismoNombre;
    }
    /*Búsqueda de Libros a partir de un titulo , Solo puede existir un titulo en concreto */
    public Libro encontrarLibro(String titulo){
        if(libros.containsKey(titulo)){
            return libros.get(titulo);
        }
        return null;
    }
    /*Calcular los gastos que tiene la biblioteca en salarios.*/
    public double calcularGastosSalarios() {
        return empleados.stream().filter(e -> e.getSalario() > 10).mapToDouble(Empleado::getSalario).sum();
    }

    @Override
    public String toString() {
        return "\nBiblioteca{" +
                "nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                "\n, libros=" + libros +
                "\n, empleados=" + empleados +
                '}';
    }

    public Biblioteca() {
        this.libros = new HashMap<String,Libro>();
        this.empleados= new Vector<>();
        nombre="sin nombre";
        direccion= "sin direccion";
    }



    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }


    public Map<String, Libro> getLibros() {
        return libros;
    }

    public void setLibros(Map<String,Libro> libros) {
        this.libros = libros;
    }

    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }


    public void addLibro(Libro libro) {
        libros.put(libro.getTitulo(),libro);
    }

    public void addEmpleado(Empleado empleado) {
        empleados.add(empleado);
    }


}
