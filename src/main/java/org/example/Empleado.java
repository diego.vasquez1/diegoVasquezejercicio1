package org.example;

public class Empleado extends  Persona{
    double salario;
    public Empleado(String name, String apellido, int edad,double salario) {
        super(name,apellido,edad);
        this.salario = salario;
    }

    public Empleado() {
        this.salario = 0;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }


    @Override
    public String toString() {
        return "Empleado{" +
                "salario=" + salario +
                " nombre="+super.getNombre()+
                " apellido="+super.getApellido()+
                " edad="+String.valueOf(super.getEdad())+
                '}';
    }
}
