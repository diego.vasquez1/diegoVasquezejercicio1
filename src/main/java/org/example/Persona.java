package org.example;
public abstract class Persona {
    private String nombre;
    private String apellido;
    private int edad;

    // Constructor por parámetros
    public Persona(String nombre, String apellido, int edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    // Constructor por defecto
    public Persona() {
        this.nombre = "";
        this.apellido = "";
        this.edad = 0;
    }

    // Getters y setters para cada atributo
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
}